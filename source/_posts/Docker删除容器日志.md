---
title: Docker删除容器日志
tags: Docker
abbrlink: c53c30d2
date: 2020-12-04 11:01:15
---

在使用docker的时候，我们常使用`docker logs -f containerId` 查看日志，但是有时候日志很多，就会带来很多麻烦，所以需要清理一下来对应容器的日志来解决该问题。

默认情况下，日志一般存放在以下的目录

```powershell
$ cd /var/lib/docker/containers/
```

使用该命令获取到容器ID

```powershell
$ docker ps -a
```

然后在`/var/lib/docker/containers/`目录下找到对应的容器ID，进去执行

```powershell
$ cd containerId
$ echo > **-json.log
```

完毕。