---
title: Vagrant安装Linux虚拟机
tags: Vagrant
abbrlink: 21192
date: 2020-08-19 10:55:30
---
<br>

### 下载装vagrant

**vagrant官方镜像仓库**

https://app.vagrantup.com/boxes/search

**vagrant下载**

https://www.vagrantup.com/downloads.html

### 初始化一个CentOS7系统

打开命令行窗口，运行

```bash
vagrant init centos/7
```

### 启动虚拟机

```bash
vagrant up # 系统root用户的密码是vagrant
```

### vagrant其他常用命令

```bash
vagrant ssh # 自动使用vagrant用户连接虚拟机
vagrant upload source [description] [name|id] # 上传文件
```

https://www.vagrantup.com/docs/cli/init.html Vagrant命令行

https://www.vagrantup.com/docs/cli/box#box-remove

### 网络配置

默认虚拟机的IP地址不是固定IP，修改Vagrantfile文件

```bash
config.vm.network "private_network", ip:"192.168.58.10"

vagrant up # 重新启动机器，然后重新vagrant连接机器
```

这里的ip需要在物理机下使用ipconfig命令找到

### 密码登录

vagrant ssh进入系统

```bash
$ vim /etc/ssh/sshd config
# 修改
PasswordAuthentication yes/no

# 重启服务
$ service sshd restart
```



### vagrant up下载box慢的解决办法

#### 即在运行vagrant up时得到其的下载路径，如：

```bash
https://cloud.centos.org/centos/7/vagrant/x86_64/images/CentOS-7-x86_64-Vagrant-2004_01.VirtualBox.box
```

然后直接在浏览器上访问该网址来下载该box

#### 先查看本地安装的box：

```bash
$ vagrant box list
```

#### 再将得到的box文件手动添加进去：

```bash
$ vagrant box add --name centos/7 /c/Users/Kings/Downloads/CentOS-7-x86_64-Vagrant-2004_01.VirtualBox.box

$ vagrant box remove centos/7 $ 从box中移除指定镜像
```

#### 然后再查看本地果然多了一个新的box：

```bash
$ vagrant box list
centos/7          (virtualbox, 0)
laravel/homestead (virtualbox, 9.5.1)
laravel/homestead (vmware_desktop, 9.2.0)
```

#### 然后再在相应vagrantfile对应的目录下运行vagrant up即可运行起来了：

```bash
$ vagrant up
```

#### 然后使用vagrant ssh即可进入：

```bash
$ vagrant ssh
```

### vagrant up 启动报错 拆坑记录 之编码设置

在 Vagrantfile 文件下加入

```bash
config.vm.provider "virtualbox" do | vb |
..............
# Encoding.default_external = 'GBK'
  Encoding.default_external = 'UTF-8'
end
```

