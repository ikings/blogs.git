---
title: Docker安装Nacos
tags: Nacos
abbrlink: 5dae1145
date: 2021-01-22 10:06:40
---

#### 前言

为了更好的管理线上、测试、开发环境中的配置，以及微服务的注册

#### 创建nacos容器并启动

```shell
$ docker run -d -p 8848:8848 \
-e MODE=standalone \
-e PREFER_HOST_MODE=hostname \
-v /root/nacos/init.d/custom.properties:/home/nacos/init.d/custom.properties \
-v /root/nacos/logs:/home/nacos/logs \
-v /root/nacos/data://home/nacos/data \
--restart always \
--name nacos nacos/nacos-server:1.4.0
```

如果服务器上没有`nacos`，那么在创建实例的时候会自动从Docker仓库拉取

这种方式的话，所有的配置文件等数据会存储到服务器磁盘上，为了更好的对配置数据管理，建议使用以下方式将所有的数据存储到`MySQL`数据库中

#### 配置MySQL数据库

##### 配置数据库

```mysql
create database nacos_config
```

##### 初始化数据库，导入初始化文件

[nacos-db.sql](https://github.com/alibaba/nacos/blob/master/config/src/main/resources/META-INF/nacos-db.sql)

#### 创建Nacos容器

```shell
$ docker run -d \
-v /data/config/nacos/init.d/custom.properties:/home/nacos/init.d/custom.properties \
-v /data/logs/nacos:/home/nacos/logs \
-v /data/data/nacos:/home/nacos/data \
-e MODE=standalone \
-e PREFER_HOST_MODE=hostname \
-e NACOS_APPLICATION_PORT=8748 \
-e SPRING_DATASOURCE_PLATFORM=mysql \
-e MYSQL_SERVICE_HOST=10.0.112.254 \
-e MYSQL_SERVICE_PORT=3306 \
-e MYSQL_SERVICE_USER=root \
-e MYSQL_SERVICE_PASSWORD=123456 \
-e MYSQL_SERVICE_DB_NAME=nacos_config \
-e JVM_XMS=512M \
-e JVM_MMS=512M \
-p 8748:8748 \
--restart=always \
--name nacos-mysql \
nacos/nacos-server:1.3.2
```

具体配置参数参考[官方文档](https://nacos.io/zh-cn/docs/quick-start-docker.html)

#### docker-compose配置

```dockerfile
nacos-mysql
	depends_on:
		- nacos-mysql
	environment:
		- MODE=standalone
        - PREFER_HOST_MODE=hostname
        - NACOS_APPLICATION_PORT=8748
        - SPRING_DATASOURCE_PLATFORM=mysql
        - MYSQL_SERVICE_HOST=10.0.112.xxx
        - MYSQL_SERVICE_PORT=3306
        - MYSQL_SERVICE_USER=root
        - MYSQL_SERVICE_PASSWORD=123456
        - MYSQL_SERVICE_DB_NAME=nacos_config
        - JVM_XMS=512M
        - JVM_MMS=512M
	volumes:
		- /data/config/custom.properties:/home/nacos/init.d/custom.properties
		- /data/logs/nacos:/home/nacos/logs
		- /data/data/nacos:/home/nacos/data
	restart: always
	container: nacos-mysql
	image: nacos/nacos-server:1.3.2
	ports: 
		- 8848:8848
```



完毕。

转载请注明出处