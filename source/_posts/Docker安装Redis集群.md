---
title: Docker安装Redis集群
tags: Docker
abbrlink: 31313
date: 2020-09-09 11:42:37
---

<br>

#### Docker搭建Redis集群

搭建3主3从redis 服务架构

#### 一、先创建6个Redis 服务

我们搭建一个3主3从集群，其中从服务主要为同步备份，主服务进行数据分片，我们为了方便快捷创建出集群，我们执行以下命令即可为我们创建出端口为7001～7006，6个redis服务。

```powershell
# 复制以下命令，创建出6个Redis服务
for port in $(seq 7001 7006);  \
do \
mkdir -p /mydata/redis/node-${port}/conf
touch /mydata/redis/node-${port}/conf/redis.conf
cat << EOF > /mydata/redis/node-${port}/conf/redis.conf
port ${port}
cluster-enabled yes
cluster-config-file nodes.conf
cluster-node-timeout 5000
cluster-announce-ip 192.168.1.119
cluster-announce-port ${port}
cluster-announce-bus-port 1${port}
appendonly yes #开启持久AOF
EOF
docker run -p ${port}:${port} -p 1${port}:1${port} \
--name redis-${port} --restart always \
-v /mydata/redis/node-${port}/data:/data \
-v /mydata/redis/node-${port}/conf/redis.conf:/etc/redis/redis.conf \
-d redis:5.0.7 redis-server /etc/redis/redis.conf; \
done

# 停止docker redis容器服务
$ docker stop $(docker ps -a|grep redis-700|awk '{print$1}') 

# 删除docker redis容器服务,加-f代表强制删除，可以删掉未停止的服务* 
$ docker rm -f $(docker ps -a|grep redis-700|awk '{print$1}')
```

**注意：**–restart always 代表随docker服务启动而自启动。如果不需要则可以删掉。另外如果设置后，可以通过docker update --restart=no 容器ID 命令修改参数为不自启动，restart参数说明如下：

- no 不自动重启
- on-failure:重启次数 指定自动重启的失败次数,到达失败次数后不再重启
- always 自动重启

```powershell
# docker update --restart=no [容器名] 
# docker update --restart=always [容器名]
# docker update --restart=on-failure:3 [容器名]
# 如需要批量修改容器参数，使用以下命令修改即可
$ docker update --restart=no  $(docker ps -a|grep redis-700|awk '{print$1}')
```

#### 二、开始配置redis集群

```powershell
# 随意进入redis服务(预设master服务)
$ docker exec -it redis-7001 /bin/bash

# 执行集群命令,--cluster-replicas 1 我们这里是一个副本
$ redis-cli --cluster create \
192.168.15.128:7001 \
192.168.15.128:7002 \
192.168.15.128:7003 \
192.168.15.128:7004 \
192.168.15.128:7005 \
192.168.15.128:7006 \
--cluster-replicas 1
```

注：

- IP地址为你创建redis容器的IP，端口为各容器开放端口，不同服务期可以端口一样，前提IP不通
- 参数–cluster-replicas 1表示给每个主节点配置一个从节点 0 代表不创建从节点

到此集群搭建完成。

#### 三、查询集群状态

```
cluster info
```

#### 四、查看集群节点信息

```
cluster nodes
```

#### 五、查看集群插槽

```powershell
cluster slots
```