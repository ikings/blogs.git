---
title: Docker如何批量删除镜像
tags: Docker
abbrlink: 3117
date: 2020-09-09 10:57:35
---

<br>

#### 一、 批量删除指定关键字的镜像

docker 使用一段时间之后，可能堆积很多用不着的，或者编译错误的镜像，一个一个删除就很麻烦，需要一个批量删除的方法，如下：

```powershell
$ docker rmi $(docker images | grep "none" | awk '{print $3}') 
```

上面这条命令，可以删除所有名字中带 “none” 关键字的镜像。

这个 grep 后面的参数，就是筛选出名字中包含这个参数的镜像。

即可以通过这条命令，删除所有名字中包含此参数的镜像。

#### 二、删除所有未运行的容器

已经运行的删除不了，未运行的就一起被删除了

```powershell
$ docker rm $(sudo docker ps -a -q)
```

#### 三、根据容器的状态，删除Exited状态的容器

```powershell
$ docker rm $(sudo docker ps -qf status=exited)
```

#### 四、删除所有的镜像

```powershell
$ docker rmi $(docker images -q)
```

#### 镜像操作

1. 创建项目Dockerfile

2. 上传项目到服务器

3. 进入项目，构建镜像到本地仓库

   3.1 docker build -t nginx:v1.0 -f ./Dockerfile . 别忘了最后小数点

   3.2 docker images 查看镜像

   3.3 docker exec -it 容器ID /bin/bash 进入容器

   3.4 docker commit -a "cqzhongxc" -m "nginx" 容器ID nginx:v1.0

#### 五、查看Docker容器的信息

```powershell
$ docker inspect tomcat001
```

