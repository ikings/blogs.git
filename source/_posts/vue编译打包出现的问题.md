---
title: vue编译打包出现的问题
tags: Vue
abbrlink: 8dc87119
date: 2020-10-19 09:30:36
---



打包到生产环境，woff字体引用的问题

打包完的 css 文件是这样的

```
/static/fonts/element-icons.535877f.woff
```

`css` 所在路径是 `/static/css/`，而加上上面的相对路径，就变成`/static/css/+/static/fonts/element-icons.535877f.woff`=`/static/css/static/fonts/element-icons.535877f.woff`导致加载出现404错误，解决办法

**版本信息**

```json
{
	"webpack": "3.6.0"
}
```

在`webpack`中可以配置`publicPath`来解决

/build/utils.js

```
if (options.extract) {
    return ExtractTextPlugin.extract({
        use: loaders,
        fallback: 'vue-style-loader',
        publicPath: '../../'
    });
} else {
    return ['vue-style-loader'].concat(loaders);
}
```



引用自

https://github.com/iview/iview/issues/515