---
title: vue模块化开发
tags: Vue
abbrlink: 61573
date: 2020-09-02 10:05:34
---


#### 设置npm淘宝镜像地址

```
npm config set registry http://registry.npm.taobao.org
```



#### Vue安装

#### 1) 初始化项目

```shell
$ npm init -y
Wrote to D:\wwwroot\npwh-vue-electron\package.json:
{
  "name": "npwh-vue-electron",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "keywords": [],
  "author": "",
  "license": "ISC"
}
```

#### 2) 安装vue

```shell
$ npm install vue
npm notice created a lockfile as package-lock.json. You should commit this file.
npm WARN npwh-vue-electron@1.0.0 No description
npm WARN npwh-vue-electron@1.0.0 No repository field.

+ vue@2.6.12
added 1 package from 1 contributor in 0.617s
```

#### Vue模块化安装

#### 1）全局安装webpack

```shell
$ npm install webpack -g
```

**webpack** 是一个用于现代 JavaScript 应用程序的*静态模块打包工具*。当 webpack 处理应用程序时，它会在内部构建一个 [依赖图(dependency graph)](https://webpack.docschina.org/concepts/dependency-graph/)，此依赖图对应映射到项目所需的每个模块，并生成一个或多个 *bundle*。

#### 2）全局安装Vue脚手架

```shell
$ npm install -g @vue/cli-init
```

CLI ( @vue/cli ) 是一个全局安装的npm 包，提供了终端里的 vue 命令。 它可以通过 vue create 快速搭建一个新项目，或者直接通过 vue serve 构建新想法的原型。 你也可以通过 vue ui 通过一套图形化界面管理你的所有项目。

#### 3）初始化Vue项目

```shell
$ vue init webpack `appname`
```

vue脚手架使用webpack模板初始化一个appname项目

#### 4）启动项目

项目的package.json中有scripts，代表我们能运行的命令

`npm start = npm run dev` 启动项目

`npm run build` 将项目打包

