---
title: Docker安装Nginx
tags: Docker
abbrlink: fb9b2386
date: 2020-10-20 17:17:24
---



Docker安装Nginx

#### 拉取镜像

```shell
$ docker pull nginx:1.19.3
```

> **随便启动一个nginx实例，只是为了复制出配置**
>
> `docker run -p 80:80 --name nginx -d nginx:1.19.3`
>
> **将容器内的配置文件拷贝到当前目录**
>
> `docker container cp nginx:/etc/nginx .`
> 
> *别忘了后面的点（.）*
>
> **修改文件名称：`mv nginx conf`  把 `conf` 移动到 `/my_data/nginx` 下**
>
> **终止原容器**
>
> `docker stop $containerId`

#### 再创建nginx实例

```shell
$ docker run -p 80:80 --name nginx \
-v /my_data/nginx/html:/usr/share/nginx/html \
-v /my_data/nginx/logs:/var/log/nginx \
-v /my_data/nginx/conf:/etc/nginx \
-d nginx:1.19.3

# 创建一个容器，
$ docker run -p 8088:8088 --name run-nginx \
--link myphpfpm:php-fpm-container \
-v /data/www:/usr/share/nginx/html \
-v /data/nginx/logs:/var/log/nginx \
-v /data/nginx/conf:/etc/nginx \
-d nginx:1.19.3

# --link 可以用来链接2个容器，使得源容器（被链接的容器）和接收容器（主动去链接的容器）之间可以相互通信，并且接收容器可以获取源容器的一些数据，如源容器的环境变量
# --link 的格式
# --link <name or id>:alias name和id是源容器，alias是源容器在link下的别名
# 注意！docker官方已不推荐使用docker run --link来链接2个容器互相通信，随后的版本中会删除--link
# 摘自 https://www.jianshu.com/p/21d66ca6115e
```

安装完毕。

