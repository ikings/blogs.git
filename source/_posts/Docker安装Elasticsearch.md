---
title: Docker安装Elasticsearch
tags: Docker
abbrlink: 2e5263b1
date: 2020-10-20 11:48:50
---

<br>

使用docker安装Elasticsearch

#### 下载镜像文件

```shell
$ docker pull elasticsearch:7.9.2
$ docker pull kibana:7.9.2
```

#### 创建`Elasticsearch`单节点实例

```shell
$ mkdir -p /my_data/elasticsearch/config
$ mkdir -p /my_data/elasticsearch/data
$ mkdir -p /my_data/elasticsearch/plugins
$ echo "http.host: 0.0.0.0" >> /my_data/elasticsearch/config/elasticsearch.yml
```

```shell
$ docker run \
--name elastic_test_node1 \
-p 9200:9200 -p 9300:9300 \
-e "discovery.type=single-node" \
-e ES_JAVA_OPTS="-Xms1024m -Xmx1024m" \
-v /my_data/cs/elasticsearch/config/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml \
-v /my_data/cs/elasticsearch/data/:/usr/share/elasticsearch/data \
-v /my_data/cs/elasticsearch/plugins:/usr/share/elasticsearch/plugins \
-d elasticsearch:7.9.2
```

> **9300与9200区别**
> 9300端口： ES节点之间通讯使用
> 9200端口： ES节点 和 外部 通讯使用

> 9300是TCP协议端口号，ES集群之间通讯端口号
> 9200端口号，暴露ES `RESTful`接口端口号
>
> `-e "discovery.type=single-node"` 单节点
>
> `-e ES_JAVA_OPTS="-Xms64m -Xmx128m"` 内存分配

#### 查看docker容器日志

```shell
$ docker logs elasticsearch
```

#### 修改挂载目录权限

```shell
$ chmod -R 777 /my_data/elasticsearch/
```

#### 安装Kibana

```shell
$ docker run \
--name kibana \
-e ELASTICSEARCH_HOSTS=http://10.0.112.254:9200 \
-p 5601:5601 \
-d kibana:7.9.2

$ docker run \
--name kibana \
-e ELASTICSEARCH_HOSTS=http://10.0.112.254:9201 \
-p 5602:5602 \
-d kibana:7.9.2

$ docker run \
--name kibana_cs \
-e ELASTICSEARCH_HOSTS=http://10.0.112.254:9211 \
-p 5603:5603 \
-d kibana:7.9.2
```

注：http://10.0.112.254:9200一定改为自己虚拟机的地址

#### 修改后自动重启

```shell
$ docker update elasticsearch --restart=always
```

#### _Cat

```
GET /_cat/nodes	查看所有节点
GET /_cat/health	查看ES健康状况
GET /_cat/master	查看主节点
GET /_cat/indices	查看所有索引   show databases;
```

全文检索按照评分进行排序，会对检索条件进行分词匹配

#### Elasticsearch集群搭建(主节点)

```shell
for port in $(seq 1 3); \
do \
mkdir -p /data/config/cs/es0${port}
mkdir -p /data/data/elastic/cs/es0${port}
mkdir -p /data/plugins/cs/es0${port}
chmod -R 777 /data
cat << EOF > /data/config/cs/es0${port}/elasticsearch.yml
cluster.name: es_cs_cluster		# 集群名称
node.name: es_cs_0${port}		# 该节点的名字
node.master: true				# 该节点有机会成为master节点
node.data: false				# 该节点可以存储数据
network.host: 0.0.0.0			# 允许所有的IP地址
http.host: 0.0.0.0				# 允许所有的IP地址均可以使用http访问
http.port: 921${port}			# 服务端口号，在同一机器的情况下必须不一样
transport.tcp.port: 931${port}	# 集群间通信端口号，在同一机器的情况下必须不一样
#discovery.zen.minimum_master_nodes: 2 # 设置这个参数来保证集群中的节点可以知道其他N个有master资格的节点。官方推荐(N/2)+1
#discovery.zen.ping_timeout: 10s # 设置集群中自动发现其他节点是ping连接的超时时间
discovery.seed_hosts: ["172.18.12.21:9311", "172.18.12.22:9312", "172.18.12.23:9313"]	# 设置集群中master节点的初始列表，可以通过这些节点来自动发现其他新加入集群的节点，es7的新增配置
cluster.initial_master_nodes: ["172.18.12.21"]	# 新集群初始的时候候选主节点，es7的新增配置
http.cors.enabled: true
http.cors.allow-origin: "*"
EOF
docker run --name es_cs_0${port} \
-p 921${port}:921${port} -p 931${port}:931${port} \
--network=esnet --ip 172.18.12.2${port} \
-e ES_JAVA_OPTS="-Xms512m -Xmx512m" \
-v /data/data/elastic/cs/es0${port}:/usr/share/elasticsearch/data \
-v /data/plugins/cs/es0${port}:/usr/share/elasticsearch/plugins \
-v /data/config/cs/es0${port}/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml \
-d elasticsearch:7.9.2
done
```

#### Elasticsearch集群搭建(数据节点)

```shell
for port in $(seq 4 6); \
do \
mkdir -p /data/config/cs/es0${port}
mkdir -p /data/data/elastic/cs/es0${port}
mkdir -p /data/plugins/cs/es0${port}
chmod -R 777 /data
cat << EOF > /data/config/cs/es0${port}/elasticsearch.yml
cluster.name: es_cs_cluster		# 集群名称
node.name: es_cs_0${port}		# 该节点的名字
node.master: false				# 该节点没有机会成为master节点
node.data: true					# 该节点可以存储数据
network.host: 0.0.0.0			# 允许所有的IP地址
http.host: 0.0.0.0				# 允许所有的IP地址均可以使用http访问
http.port: 921${port}			# 服务端口号，在同一机器的情况下必须不一样
transport.tcp.port: 931${port}	# 集群间通信端口号，在同一机器的情况下必须不一样
#discovery.zen.minimum_master_nodes: 2 # 设置这个参数来保证集群中的节点可以知道其他N个有master资格的节点。官方推荐(N/2)+1
#discovery.zen.ping_timeout: 10s # 设置集群中自动发现其他节点是ping连接的超时时间
discovery.seed_hosts: ["172.18.12.21:9311", "172.18.12.22:9312", "172.18.12.23:9313"]	# 设置集群中master节点的初始列表，可以通过这些节点来自动发现其他新加入集群的节点，es7的新增配置
cluster.initial_master_nodes: ["172.18.12.21"]	# 新集群初始的时候候选主节点，es7的新增配置
http.cors.enabled: true			# 是否允许跨域请求。默认为false
http.cors.allow-origin: "*"		# 定义允许哪些源请求。可以使用正则表达式，例如/https?:\/\/localhost(:[0-9]+)?/可设置支持本地HTTP和HTTPS请求。也可以设置为*,但是会存在安全隐患，因为任何来源都可访问Elasticsearch(以下简称为Es)实例。
EOF
docker run --name es_cs_0${port} \
-p 921${port}:921${port} -p 931${port}:931${port} \
--network=esnet --ip 172.18.12.2${port} \
-e ES_JAVA_OPTS="-Xms512m -Xmx512m" \
-v /data/data/elastic/cs/es0${port}:/usr/share/elasticsearch/data \
-v /data/plugins/cs/es0${port}:/usr/share/elasticsearch/plugins \
-v /data/config/cs/es0${port}/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml \
-d elasticsearch:7.9.2
done
```

```shell
for indx in $(seq 1 3); \
do \
docker rm $(docker stop es_cs_0${indx})
done

for indx in $(seq 4 6); \
do \
docker rm $(docker stop es_cs_0${indx})
done

for indx in $(seq 1 6); \
do \
docker rm $(docker stop es_cs_0${indx})
done

for indx in $(seq 1 6); \
do \
docker rm es_cs_0${indx}
done

for indx in $(seq 1 6); \
do \
docker start es_cs_0${indx}
done

for indx in $(seq 1 6); \
do \
docker stop es_cs_0${indx}
done

for indx in $(seq 1 6); \
do \
docker start $(docker stop es_cs_0${indx})
done
```

#### ElasticSearch 配置文件

https://coyotey.gitbooks.io/elasticsearch/content/chapter5.html