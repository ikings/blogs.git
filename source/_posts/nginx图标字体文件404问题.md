---
title: nginx图标字体文件404问题
tags: nginx
abbrlink: 74cfff3f
date: 2020-10-19 09:22:53
---



前端项目部署好之后，打开发现字体文件以及图标文件404错误

**解决办法**

```nginx
location ~* \.(eot|otf|ttf|woff)$ {
    root       html;
    add_header Access-Control-Allow-Origin *;
}
```

再次重启nginx，加载成功