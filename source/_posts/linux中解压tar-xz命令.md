---
title: linux中解压tar.xz命令
tags: CentOS
abbrlink: 14ac4516
date: 2020-11-10 10:03:02
---

对于tar.xz结尾的压缩文件，解压的方式有很多种，这里记录其中一种

**直接使用如下命令解压**

```powershell
$ tar xvJf ***.tar.xz
```

