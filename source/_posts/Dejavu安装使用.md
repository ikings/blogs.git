---
title: ElasticSearch可视化工具Dejavu安装使用
tags: ElasticSearch
abbrlink: 33460
date: 2020-09-09 10:36:27
---

Dejavu 是一个 ElasticSearch 的 Web UI 工具，支持通过 JSON 和 CSV 文件导入数据，支持可视化定义 Mapping (字段映射)等。

相关描述在 https://github.com/appbaseio/dejavu/ 上都有详细描述，这里只是做个简单的记录。

#### 1、安装 Docker 环境

这里就不细述了，网上有很多相关的资料。

- [CentOS Docker 安装](https://www.runoob.com/docker/centos-docker-install.html)
- [Ubuntu Docker 安装](https://www.runoob.com/docker/ubuntu-docker-install.html)
- [Docker 镜像加速](https://www.runoob.com/docker/docker-mirror-acceleration.html)

也可以不使用 Docker ，会比较麻烦一点。

#### 2、运行 ElasticSearch 服务

如果已经存在 **ElasticSearch** 服务，那么只需要确保其启用了 **CORS** (跨域)设置即可。

启用方式为，在 `elasticsearch.yml` 配置文件中添加以下几行：

```yml
http.port: 9200
http.cors.allow-origin: 'http://localhost:1358'
http.cors.enabled: true
http.cors.allow-headers: X-Requested-With,X-Auth-Token,Content-Type,Content-Length,Authorization
http.cors.allow-credentials: true
```

如果不存在 **ElasticSearch** 服务的情况下，则可运行下面命令运行一个 **ElasticSearch** 的 **docker** 容器

**ElasticSearch 2.x**

```powershell
docker run --name elasticsearch -p 9200:9200 -d elasticsearch:2 -Des.http.port=9200 -Des.http.cors.allow-origin="http://localhost:1358" -Des.http.cors.enabled=true -Des.http.cors.allow-headers=X-Requested-With,X-Auth-Token,Content-Type,Content-Length,Authorization -Des.http.cors.allow-credentials=true
```

**ElasticsSarch 5.x**

```
docker run --name elasticsearch -p 9200:9200 -d elasticsearch:5 -E http.port=9200 -E http.cors.allow-origin="http://localhost:1358" -E http.cors.enabled=true -E http.cors.allow-headers=X-Requested-With,X-Auth-Token,Content-Type,Content-Length,Authorization -E http.cors.allow-credentials=true
```

**ElasticSearch 6.x**

```
docker run -p 9200:9200 -d elasticsearch docker.elastic.co/elasticsearch/elasticsearch-oss:6.5.4 -Ehttp.port=9200 -Ehttp.cors.enabled=true -Ehttp.cors.allow-origin=http://localhost:1358 -Ehttp.cors.allow-headers=X-Requested-With,X-Auth-Token,Content-Type,Content-Length,Authorization -Ehttp.cors.allow-credentials=true
```

**ElasticSearch 7.x**

```
docker run -d --rm --name elasticsearch -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" -e "http.cors.enabled=true" -e "http.cors.allow-origin=*" -e "http.cors.allow-headers=X-Requested-With,X-Auth-Token,Content-Type,Content-Length,Authorization" -e "http.cors.allow-credentials=true" docker.elastic.co/elasticsearch/elasticsearch-oss:7.0.1
```

安装完成后可以使用下面命令创建一个新索引

```
curl -X PUT http://ip:9200/test
```

#### 3、安装运行 Dejavu 服务

使用 **docker** 安装 **Dejavu** 服务

```
docker run -p 1358:1358 -d appbaseio/dejavu
```

安装完成后可以使用浏览器打开 `http://ip:1358` 使用。

![](/assets/images/dejavu1.png)

