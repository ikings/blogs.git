---
title: JDK环境配置
tags: Java
abbrlink: 835ea187
date: 2020-11-06 10:03:55
---

1. 查看已经安装的 JAVA 版本信息

   ```shell
   java -version
   ```

2. 查看 JDK 的信息

   ```shell
   rpm -qa | grep java
   ```

3. 卸载已经安装的 JAVA

   ```shell
   yum remove java-1.6.0.xxxxxxx.xxx.xx
   ```

4. 卸载另外一个

   ```shell
   yum remove tzdata-java-xxxxx.x.xxx.xx
   ```

5. 安装并配置 JDK

   ```shell
   vi /etc/profile
   # 在文件最尾添加
   JAVA_HOME=/usr/local/jdk1.8.0_261
   JRE_HOME=$JAVA_HOME/jre
   CLASSPATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
   PATH=$PATH:$JAVA_HOME/bin

   export JAVA_HOME
   export JRE_HOME
   export CLASSPATH
   export PATH

   source /etc/profile
   ```
