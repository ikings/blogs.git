---
title: CentOS7启动nginx报错
tags: CentOS
abbrlink: e7db5d6
date: 2021-02-02 09:19:14
---

`CentOS7`启动报错：error while loading shared libraries: `libpcre.so.0`

首先确认`pcre`是否已安装，如果没有安装则需要先安装一下

#### 安装 `gcc+` 和 `gcc-c++`

```powershell
$ yum -y install gcc+ gcc-c++
```

#### 安装`pcre`库

```powershell
$ wget https://ftp.pcre.org/pub/pcre/pcre-8.00.tar.gz
$ tar zxvf pcre-8.00.tar.gz
$ cd pcre-8.00
$ ./configure --disable-shared --enable-utf8
$ make && make check && make install
```

#### 检查 `libpcre` 文件是否存在

```powershell
$ cd /lib64/
$ ls -ld libpcre.so.*
```

#### 创建软连接

```powershell
$ ln -s /lib64/libpcre.so.1.2.0 /lib64/libpcre.so.0
```

现在进行`nginx/sbin`目录，执行`./nginx`

完毕