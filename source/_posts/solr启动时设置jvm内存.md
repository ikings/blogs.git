---
title: solr启动时设置jvm内存
tags: Solr
abbrlink: 93d9d9e4
date: 2020-10-19 16:03:30
---

#### solr启动时设置jvm内存，防止大数据量查询等操作崩溃

```
./solr start -p 8987 -force -m 2g
```

为虚拟机分配2g内存





https://github.com/zenghao0219/seat-select