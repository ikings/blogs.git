---
title: spring-boot-maven-plugin not found的解决方案
tags: SpringBoot
abbrlink: fd18dcd1
date: 2020-11-06 10:45:47
---



### 使用IDEA创建SpringBoot项目

```xml
<plugin>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-maven-plugin</artifactId>
</plugin>
```

提示spring-boot-maven-plugin插件not fund

**spring-boot-maven-plugin指定版本后成功解决**

```xml
<plugin>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-maven-plugin</artifactId>
    <version>2.3.5.RELEASE</version>
</plugin>
```

