---
title: 'nohup: ignoring input and redirecting stderr to stdout'
tags: CentOS
abbrlink: 81a98e70
date: 2021-01-22 09:37:53
---

#### 服务器上部署jar包

```shell
$ nohup java -jar xxx.jar --spring.profiles.active=prod > xxx.log &
```

#### 出现警告提示

```
nohup: ignoring input and redirecting stderr to stdout
```

#### 解决办法

```shell
$ nohup java -jar xxx.jar --spring.profiles.active=prod > xxx.log 2> &1 &
```

> 2> ：表示把标准错误重定向，标准输出是1

> 尖括号后面可以跟文件名，或者 &1，&2，分别表示重定向到标准输出和标准错误

> 2>&1

> 1>&2

> 2>xxx.log

> 1>xxx.log