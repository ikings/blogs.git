---
title: alpine使用的避坑指南
tags: Alpine
abbrlink: 64969
date: 2020-08-18 22:53:57
---
#### alpine使用的避坑指南

alpine，是一个重量仅为5 MB的最小Linux发行版。它还有基本的linux工具和一个不错的包管理器APK。APK非常稳定，有相当数量的包。由于体积小，在容器中很受欢迎，但是使用上坑也很多，大部分可能是我们的无知吧。

#### 坑1 ： 下载慢
解决：
```powershell
echo "https://mirror.tuna.tsinghua.edu.cn/alpine/v3.4/main" > /etc/apk/repositories \
&& echo "https://mirror.tuna.tsinghua.edu.cn/alpine/v3.4/community" >> /etc/apk/repositories \
&& echo "https://mirror.tuna.tsinghua.edu.cn/alpine/edge/testing" >> /etc/apk/repositories
```

#### 坑2： 找不到包
解决： 搜索 https://pkgs.alpinelinux.org/packages ，加入指定的“Branch”和“Repository”的源到/etc/apk/repositories，就可以apk add

#### 坑3： 安装失败
```
ERROR: https://mirror.tuna.tsinghua.edu.cn/alpine/v3.4/community: Bad file descriptor
WARNING: Ignoring APKINDEX.79f426e4.tar.gz: Bad file descriptor
ERROR: unsatisfiable constraints:
  openssh (missing):
    required by: world[openssh]
```
解决：
```
apk add  openssh --no-cache #加--no-cache, 另外，还能减少镜像体积
```

#### 坑4：镜像体积大，想删除不再使用的包
apk add 加 -t 参数 

-t, --virtual NAME Instead of adding all the packages to 'world', create a new virtual package with the listed dependencies and add that to 'world'; the
actions of the command are easily reverted by deleting the virtual package
这意味着当您安装软件包时，这些软件包不会添加到全局软件包中。这种变化可以很容易地回滚/删除。所以，如果我需要gcc来编译程序，但是一旦程序被编译，我就不再需要gcc了。

我可以在虚拟包中安装gcc和其他必需的包，并且可以删除所有依赖项，并删除此虚拟包名称。以下是示例用法

apk add --virtual mypacks gcc vim
apk del mypacks
使用第一个命令安装的所有18个软件包将被下一个命令删除。

注意：同一个-t参数会覆盖之前的所有安装包，所有对动态链接库最好不使用-t ,或者保证此参数不重复。

#### 坑5：时间不同步
```
echo "Asia/Shanghai" > /etc/timezone
apk add –no-cache tzdata
TZ=Asia/Shanghai
ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
```

#### 坑6：用户（组）和宿主机不兼容
解决：
```
addgroup -g 1200 -S www \
     && adduser -u 1200 -D -S -G www www
```

#### 坑7：不兼容glibc
解决：
使用最新稳定版本的alpine,
```
#再安装编译环境
apk add --virtual .build-base --no-cache \
autoconf \
automake \
g++ \
make \
linux-headers \
bsd-compat-headers
```