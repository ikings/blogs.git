---
title: CentOS7常见问题
tags: CentOS
abbrlink: 8136
date: 2020-08-18 17:25:06
---

#### ifconfig command not found解决和netstat -an
```
# 没有 ifconfig 和netstat -an 的话安装 net-tools package
$ yum install net-tools
```

#### 检查端口被哪个进程占用
```
netstat -lnp | grep 8000
```

#### 查看进程的详细信息
```
ps 11100
```

#### 杀掉进程
```
kill -9 11100
```