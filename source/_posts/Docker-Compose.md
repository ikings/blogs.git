---
title: Docker Compose
tags: Docker
abbrlink: '60641507'
date: 2021-01-27 13:42:13
---

#### 前言

Compose 是用于定义和运行多容器 Docker 应用程序的工具。通过 Compose，您可以使用 `YML` 文件来配置应用程序需要的所有服务。然后，使用一个命令，就可以从 `YML` 文件配置中创建并启动所有服务。

#### Compose使用步骤

- 使用`Dockerfile`定义应用程序的环境
- 使用`docker-compose.yml`定义构成应用程序的服务

- 执行`docker-compose up`命令来启动并运行

#### Compose安装

从github下载，最新版本地址

https://github.com/docker/compose/releases

运行以下命令下载当前稳定版本

```shell
$ sudo curl -L https://github.com/docker/compose/releases/download/1.25.5/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
```

或者

```shell
$ sudo curl -L https://get.daocloud.io/docker/compose/releases/download/1.25.5/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
```

新增可执行权限

```shell
$ sudo chmod +x /usr/local/bin/docker-compose
```

测试是否安装成功

```shell
$ docker-compose --version
```

#### Compose命令说明

执行`docker-compose [COMMAND] --help`查看具体某个命令的使用说明

基本使用格式

```shell
$ docker-compose [options] [COMMAND] [.....]
```

选项

- `--verbose` 输出更多调试信息。
- `--version` 打印版本并退出。
- `-f, --file FILE` 使用特定的 compose 模板文件，默认为 `docker-compose.yml`。
- `-p, --project-name NAME` 指定项目名称，默认使用目录名称

命令

`build`

构建或重新构建服务

可以随时在项目目录下运行`docker-compose build`来重新构建服务

`help`

获得一个命令的帮组

`kill`

通过发送 `SIGKILL` 信号来强制停止服务容器。支持通过参数来指定发送的信号，例如

```shell
$ docker-compose kill -s SIGINT
```

`logs`

查看服务日志输出

`port`

打印绑定的公共端口

`ps`

列出所有容器

`pull`

拉取服务镜像

`rm`

删除停止的服务容器

`start`

启动一个已经存在的服务容器

`stop`

停止一个已经运行的容器，但不删除它。通过 `docker-compose start` 可以再次启动这些容器

`up`

构建，（重新）创建，启动，链接一个服务相关的容器。

链接的服务都将会启动，除非他们已经运行。

默认情况， `docker-compose up` 将会整合所有容器的输出，并且退出时，所有容器将会停止。

如果使用 `docker-compose up -d` ，将会在后台启动并运行所有的容器。

默认情况，如果该服务的容器已经存在， `docker-compose up` 将会停止并尝试重新创建他们（保持使用 `volumes-from` 挂载的卷），以保证 `docker-compose.yml` 的修改生效。如果你不想容器被停止并重新创建，可以使用 `docker-compose up --no-recreate`。如果需要的话，这样将会启动已经停止的容器