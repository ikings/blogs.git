---
title: Mybatis使用IN语句查询
tags: MyBatis
abbrlink: 24cb40d
date: 2021-01-28 09:22:32
---

<br>

Mybatis中提供了foreach语句实现IN查询，具体语法如下：

```
foreach语句中
	collection 参数类型：List、数组、map集合，必须跟@Param标签指定的元素名一样
	item： 表示在迭代过程中每一个元素的别名，可以随便起名，但是必须跟元素中的#{}里面的名称一样。
　　 index：表示在迭代过程中每次迭代到的位置(下标)
　　 open：前缀，sql语句中集合都必须用小括号()括起来
　　 close：后缀
　　 separator：分隔符，表示迭代时每个元素之间以什么分隔
```

#### 实例如下：

**selectByIds(@Param("ids") Collection<? extends Serializable> ids)**

```xml
<select id="selectByIds" resultType="com.xxx.xxxEntity">
    SELECT * FROM xxx WHERE id IN
    <foreach collection="ids" item="id" index="index" open="(" close=")" separator=",">
    	#{id}
    </foreach>
</select>
```

