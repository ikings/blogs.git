---
title: VSCode新建vue模版
tags: Vue
abbrlink: 10468
date: 2020-09-03 14:39:18
---

<br>

#### 前言

每次新建一个vue文件！都要重行敲打一遍template。script。style。神烦！有没有和别的编辑器一样！设置自己默认的模板！

#### 一、安装vscode

官网下载地址

```
https://code.visualstudio.com/
```

#### 二、安装一个插件，识别vue文件

```
插件库中搜索Vetur，下图中的第一个，点击安装，安装完成之后点击重新加载
```

#### 三、新建代码片段

```
文件-->首选项-->用户代码片段-->点击新建代码片段--取名vue.json 确定
```

![](/assets/images/vetur.png)

#### 四、粘入自己写的.vue模板

```json
{
    "Print to console": {
        "prefix": "vue",
        "body": [
            "<!-- $1 -->",
            "<template>",
            "<div class='$2'>$5</div>",
            "</template>",
            "",
            "<script>",
            "// 这里可以导入其他文件（比如：组件，工具js，第三方插件js，json文件，图片文件等等）",
            "// 例如：import 《组件名称》 from '《组件路径》';",
            "",
            "export default {",
            "// import引入的组件需要注入到对象中才能使用",
            "components: {},",
            "data() {",
            "// 这里存放数据",
            "return {",
            "",
            "};",
            "},",
            "// 监听属性 类似于data概念",
            "computed: {},",
            "// 监控data中的数据变化",
            "watch: {},",
            "// 方法集合",
            "methods: {",
            "",
            "},",
            "// 生命周期 - 创建完成（可以访问当前this实例）",
            "created() {",
            "",
            "},",
            "// 生命周期 - 挂载完成（可以访问DOM元素）",
            "mounted() {",
            "",
            "},",
            "beforeCreate() {}, // 生命周期 - 创建之前",
            "beforeMount() {}, // 生命周期 - 挂载之前",
            "beforeUpdate() {}, // 生命周期 - 更新之前",
            "updated() {}, // 生命周期 - 更新之后",
            "beforeDestroy() {}, // 生命周期 - 销毁之前",
            "destroyed() {}, // 生命周期 - 销毁完成",
            "activated() {}, // 如果页面有keep-alive缓存功能，这个函数会触发",
            "}",
            "</script>",
            "<style lang='scss' scoped>",
            "// @import url($3); 引入公共css类",
            "$4",
            "</style>"
        ],
        "description": "Log output to console"
    }
}
```

**上面代码中的 "prefix": "vue", 就是快捷键；保存好之后，新建.vue结尾的文件试试**

```undefined
输入vue 按键盘的tab就行
```

模板创建完毕。