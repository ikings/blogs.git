---
title: Docker安装MySQL
tags: Docker
abbrlink: 61234
date: 2020-08-18 16:15:52
---

<br>

#### 前言

为了更好的管理，打算把MySQL、Redis等服务放在虚拟机中统一部署，这样不会因为这些服务的问题影响到系统本身。前段时间正好在看docker相关的内容，打算在虚拟机中通过docker来使用MySQL等服务。

#### 一、下载镜像文件

```powershell
$ docker pull mysql:5.7
```

#### 二、查看镜像

```
$ docker images
```

#### 三、创建实例并启动

```powershell
$ docker run -p 3306:3306 --name=mysql57 \
-v /data/mysql/log:/var/log/mysql \
-v /data/mysql/data:/var/lib/mysql \
-v /data/mysql/conf/:/etc/mysql \
-e TZ=Asia/Shanghai \
-e MYSQL_ROOT_PASSWORD=123456 \
-d mysql:5.7

# 参数说明
--restart=always # 当docker重启时，该容器自动重启
--name mysql  # 将容器命名为mysql，后面可以用这个name进行容器的启动暂停等操作
-e MYSQL_ROOT_PASSWORD=123456 # 设置MySQL密码为123456
-d # 此容器在后台运行,并且返回容器的ID
-i # 以交互模式运行容器
-p 3306:3306 # 将容器的3306端口映射到主机的3306端口
-v /data/mysql/log:/var/log/mysql # 将配置文件夹挂载到主机
-v /data/mysql/data:/var/lib/mysql # 将日志文件夹挂载到主机
-v /data/mysql/conf/:/etc/mysql # 将配置文件夹挂载到主机
-e TZ=Asia/Shanghai # 时区设置
```

#### 四、修改配置

```powershell
# 进入配置目录
$ cd /data/mysql/conf

# 修改数据库配置
$ vim my.cnf
[client]
default-character-set=utf8
[mysql]
default-character-set=utf8
[mysqld]
init_connect='SET collation_connection = utf8_unicode_ci'
init_connect='SET NAMES utf8'
character-set-server=utf8
collation-server=utf8_unicode_ci
skip-character-set-client-handshake
skip-name-resolve

# 重启 mysql57 容器
$ docker restart mysql57
```

#### 五、设置启动docker时，即运行mysql

```powershell
$ docker update mysql --restart=always
```

#### 六、启动docker容器的时候报错

```powershell
$ docker start mysql57
Error response from daemon: driver failed programming external connectivity on endpoint mysql57
Error: failed to start containers: mysql57
```

**解决方法：重启docker**

```powershell
$ systemctl restart docker
```

安装完毕。



