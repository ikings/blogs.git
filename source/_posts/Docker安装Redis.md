---
title: Docker安装Redis
tags: Docker
abbrlink: 41794
date: 2020-08-19 17:28:37
---

<br>

#### Docker安装Redis

Redis 是一个开源的使用 ANSI C 语言编写、支持网络、可基于内存亦可持久化的日志型、Key-Value 的 NoSQL 数据库，并提供多种语言的 API。

#### 一、下载镜像

```powershell
$ docker pull redis:6.0.6
```

#### 二、创建文件夹以及配置文件

```powershell
$ mkdir -p /data/redis/conf
$ touch /data/redis/conf/redis.conf
```

#### 三、创建实例并启动

```powershell
$ docker run -p 6379:6379 --name redis606 \
-v /data/redis/conf/redis.conf:/etc/redis/redis.conf \
-d redis:6.0.6 \
redis-server \
/etc/redis/redis.conf
```

#### 四、接着我们通过 redis-cli 连接测试使用 redis 服务。

```powershell
$ docker exec -it redis606 /bin/bash
```

#### 五、设置Redis容器在docker启动的时候启动

```powershell
$ docker update redis --restart=always
```

安装完毕。