---
title: PHP截取汉字乱码问题
tags: PHP
abbrlink: b7a4d06a
date: 2021-02-04 17:40:11
---

利用 `mb_substr` 函数截取字符串

**代码如下**

```php
<?php
	$str = '鹰潭市人民医院';
	echo mb_substr($str, 0, 4, 'utf-8');
	// 鹰潭市人民
?>
```

注：如果截取的是中文，务必加上第三个参数 `utf-8`

