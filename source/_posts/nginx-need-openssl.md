---
title: Nginx 编译报错：SSL modules require the OpenSSL library
tags: Nginx
abbrlink: 9273
date: 2020-09-11 09:24:56
---

<br>

#### 一、编译报错

**./configure: error: SSL modules require the OpenSSL library.**

```powershell
./configure: error: SSL modules require the OpenSSL library.
You can either do not enable the modules, or install the OpenSSL library
into the system, or build the OpenSSL library statically from the source
with nginx by using --with-openssl=<path> option.
```

在编译Nginx的时候遇到这样的错误，环境缺少OpenSSL，需要手动安装

**CentOS**

```
$ yum -y install openssl openssl-devel
```

**Ubuntu**

```
$ yum -y install openssl openssl-devel
```

但是使用 `yum install openssl` 后，再次编译依然会报错

```
or build the OpenSSL library statically from the source
with nginx by using --with-openssl=<path> option.
```

按以上报错的提示，需要手动指定OpenSSL的安装位置

#### 二、OpenSSL编译安装

**官网下载地址**

https://www.openssl.org/source/openssl-1.1.1g.tar.gz

下载后解压

```powershell
$ tar zxvf openssl-1.1.1g.tar.gz
$ cd openssl-1.1.1g
```

配置编译安装

```powershell
$ ./config --prefix=/usr/local/openssl
$ make && make install
```

如果要替换`yum install openssl`安装的，请执行以下操作

```powershell
$ mv /usr/bin/openssl /usr/bin/openssl.old
$ mv /usr/include/openssl /usr/include/openssl.old
$ ln -s /usr/local/openssl/bin/openssl /usr/bin/openssl
$ ln -s /usr/local/openssl/include/openssl /usr/include/openssl
$ ln -s /usr/local/openssl/lib/libssl.so.1.1 /usr/lib64/libssl.so.1.1
$ ln -s /usr/local/openssl/lib/libcrypto.so.1.1 /usr/lib64/libcrypto.so.1.1
```

## 三、再次编译安装Nginx

```powershell
$ ./configure --prefix=/usr/local/nginx \
--with-openssl=/usr/local/openssl \
--without-http_rewrite_module \
--without-http_gzip_module \
--with-http_ssl_module \
--with-pcre


./configure --prefix=/usr/local/nginx \
--with-http_gzip_static_module \
--with-http_v2_module \
--with-http_ssl_module \
--with-pcre
```

**fatal error: `openssl/ssl.h`: No such file or directory**

要解决这个问题，你需要安装 OpenSSL 开发包，执行以下命令进行安装，然后重新编译即可解决

```powershell
$ sudo yum install openssl-devel
```





```
CORE_DEPS="$CORE_DEPS $OPENSSL/openssl/include/openssl/ssl.h"

CORE_INCS="$CORE_INCS $OPENSSL/.openssl/include"
CORE_DEPS="$CORE_DEPS $OPENSSL/.openssl/include/openssl/ssl.h"
CORE_LIBS="$CORE_LIBS $OPENSSL/.openssl/lib/libssl.a"
CORE_LIBS="$CORE_LIBS $OPENSSL/.openssl/lib/libcrypto.a"
CORE_LIBS="$CORE_LIBS $NGX_LIBDL"

```







```
    server {
        listen       443 ssl;
        server_name  zhongxc.cc www.zhongxc.cc;

        ssl_certificate      ssl/fullchain.cer;
        ssl_certificate_key  ssl/zhongxc.cc.key;

    #    ssl_session_cache    shared:SSL:1m;
    #    ssl_session_timeout  5m;

    #    ssl_ciphers  HIGH:!aNULL:!MD5;
    #    ssl_prefer_server_ciphers  on;

        location / {
            root   html;
            index  index.html index.htm;
        }
    }
/usr/local/nginx/conf/conf.d
```

```properties
# openresty：已经安装了openssl，但是执行 ./configure时还是一直报：./configure: error: SSL modules require the OpenSSL library.
https://www.cnblogs.com/Guhongying/p/11636369.html

# centos中编译安装nginx并支持ssl
https://blog.csdn.net/bjnihao/article/details/52370089

# 已安装nginx支持https配置 the "ssl" parameter requires ngx_http_ssl_module
https://blog.csdn.net/GMingZhou/article/details/104490684
```

```


```

#### 快速设置— 如果你知道该怎么操作，直接使用下面的地址

https://gitee.com/ikings/blogs.git

我们强烈建议所有的git仓库都有一个`README`, `LICENSE`, `.gitignore`文件

Git入门？查看 [帮助](https://gitee.com/oschina/git-osc/wikis/帮助) , [Visual Studio](https://gitee.com/help/articles/4118) / [TortoiseGit](http://my.oschina.net/longxuu/blog/141699) / [Eclipse](https://gitee.com/help/articles/4119) / [Xcode](http://my.oschina.net/zxs/blog/142544) 下如何连接本站, [如何导入仓库](http://www.oschina.net/question/82993_133520)

#### 简易的命令行入门教程:

Git 全局设置:

```
git config --global user.name "king's"
git config --global user.email "cqzhongxc@hotmail.com"
```

创建 git 仓库:

```
mkdir blogs
cd blogs
git init
touch README.md
git add README.md
git commit -m "first commit"
git remote add origin https://gitee.com/ikings/blogs.git
git push -u origin master
```

已有仓库?

```
cd existing_git_repo
git remote add origin https://gitee.com/ikings/blogs.git
git push -u origin master
```