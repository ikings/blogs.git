---
title: IDEA中配置使用Docker
tags: Docker
abbrlink: 3d46b3f8
date: 2021-01-20 15:06:03
---

Docker允许开发人员在容器中部署应用程序，以便在生产环境相同的环境中测试代码，IDEA使用Docker插件提供Docker支持。

#### CentOS7中Docker配置

修改Docker配置，让其开发Docker Remote API

编辑Docker配置文件

```sh
$ vim /usr/lib/systemd/system/docker.service
```

在该行添加如下内容

```shell
# 端口2375，在idea中连接时需要填写该端口
ExecStart=/usr/bin/dockerd -H tcp://0.0.0.0:2375 -H unix://var/run/docker.sock
```

重新加载配置文件

```shell
$ systemctl daemon-reload
```

重启Docker

```shell
$ systemctl restart docker
```

#### IDEA中配置Docker

步骤如下

- 打开IDEA设置：File > Settings > Build, Execution, Deployment > Docker

- 点击 + 添加Docker配置，在此配置如何连接到Docker守护进程

连接的设置取决于Docker版本和操作系统

- Docker for Windows\Mac：如果你本地安装了Docker for Windows\Mac，则推荐此连接选项
- Docker Machine：如果您使用的是Docker Toolbox for Windows，建议使用此选项连接Docker API。
- TCP socket：如果你本机并未安装任何docker，也可以使用此选项来连接远程主机上的docker
  - Engine API URL：取决于Docker版本和操作系统
    - Docker for Windows: `tcp://192.158.1.111:2375` 
    - Docker for macOS 或 Linux: `unix:///var/run/docker.sock`
    - Docker Toolbox for Windows 或 macOS: `https://192.168.99.100:2376`

